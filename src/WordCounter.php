<?php

namespace Drupal\word_counter;

/**
 * The service for calculation of word count.
 */
class WordCounter implements WordCounterInterface {

  /**
   * {@inheritdoc}
   */
  public function getWordCount($string) {
    $plain_text = $this->stripAllTags($string);
    $words = preg_split('/\s+/', $plain_text);
    return count($words);
  }

  /**
   * Properly strip all HTML tags including script and style.
   *
   * @param string $string
   *   String containing HTML tags.
   * @param bool $remove_breaks
   *   Whether to remove left over line breaks and white space chars.
   *
   * @return string
   *   The processed string.
   *
   * @see https://developer.wordpress.org/reference/functions/wp_strip_all_tags/
   */
  private function stripAllTags($string, $remove_breaks = FALSE) {
    $string = preg_replace('@<(script|style)[^>]*?>.*?</\\1>@si', '', $string);
    $string = strip_tags($string);

    if ($remove_breaks) {
      $string = preg_replace('/[\r\n\t ]+/', ' ', $string);
    }

    return trim($string);
  }

}
