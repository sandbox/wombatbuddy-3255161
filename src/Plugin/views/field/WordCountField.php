<?php

namespace Drupal\word_counter\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\word_counter\WordCounterInterface;
use Drupal\views\ResultRow;

/**
 * Field handler for field_word_counter.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("field_word_counter")
 */
class WordCountField extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Service word_counter.
   *
   * @var \Drupal\word_counter\WordCounterInterface
   */
  protected $wordCounter;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WordCounterInterface $word_counter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->wordCounter = $word_counter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('word_counter')
    );
  }

  /**
   * Do nothing.
   */
  public function query() {
  }

  /**
   * Render word counter in views field.
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;

    if ($node->bundle() == 'article') {
      return $this->wordCounter->getWordCount($node->body->value);
    }
  }

}
