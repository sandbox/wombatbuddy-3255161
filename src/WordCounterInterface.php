<?php

namespace Drupal\word_counter;

/**
 * Interface of word_counter service.
 */
interface WordCounterInterface {

  /**
   * Calculate a word count of a string.
   *
   * @param string $string
   *   A string containing words.
   *
   * @return int
   *   The word count.
   */
  public function getWordCount($string);

}
